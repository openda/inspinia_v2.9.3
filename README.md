# inspinia_v2.9.3 —— 响应式管理模板

#### 介绍
IINSPINIA Admin Theme是具有平面设计概念的仪表板模板。它是使用最新的Bootstrap 4.x Framework，HTML5，CSS3和Javascript构建的完全响应式管理仪表板模板。它具有大量可重复使用的UI组件以及为特殊功能而集成的数十个第三方库。

#### 目录结构
```
ASPNET_Core_3_0_Full_Project
ASPNET_Core_3_0_Seed_Project
HTML5_Full_Version
HTML5_Seed_Project
Landing_Page
Rails_6_0_Seed_Project
Rails_6_0_Full_Project
Utils
```
注：内容来源于网络。